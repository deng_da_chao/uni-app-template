import Mock from 'mockjs'

module.exports = [
  {
    url: '/mock/test',
    result: (data) => {
      return {
        code: 200,
        msg: 'success',
        data
      }
    }
  }
]
