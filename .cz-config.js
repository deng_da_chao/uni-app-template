module.exports = {
  types: [
    { value: 'init', name: '☀️   初始化:  初始提交' },
    { value: 'feat', name: '✨  特性:    新增功能' },
    { value: 'fix', name: '🐛  修复:    修复一个Bug' },
    { value: 'docs', name: '📝  文档:    文档变更' },
    { value: 'format', name: '💄  format:  代码格式（不影响功能，例如空格、分号等格式修正）' },
    { value: 'style', name: '🎨  样式:    样式修改不影响逻辑' },
    { value: 'del', name: '❌  删除:    删除代码/文件' },
    { value: 'refactor', name: '♻️   重构:    代码重构，注意和特性、修复区分开' },
    { value: 'perf', name: '⚡️  性能:    提升性能' },
    { value: 'test', name: '✅  测试:    添加一个测试' },
    { value: 'tool', name: '🔧  工具:    开发工具变动(构建、脚手架工具等)' },
    { value: 'revert', name: '⏪  回滚:    代码回退' }
  ],
  /*  scopes: [
    {name: 'src'},
    {name: 'components'},
    {name: 'view/page'},
    {name: 'utils'},
    {name: '模块4'}
  ], */
  // override the messages, defaults are as follows
  messages: {
    type: '选择一种你的提交类型(必选):',
    scope: '选择代码影响范围 (可选):',
    // used if allowCustomScopes is true
    customScope: '指示此更改的范围(可选):',
    subject: '短说明:\n ',
    body: '长说明，使用" | "换行(可选)：\n',
    breaking: '非兼容性说明 (可选):\n',
    footer: '关联关闭的issue，例如：#31, #34(可选):\n',
    confirmCommit: '确定提交?'
  },
  allowCustomScopes: true,
  allowBreakingChanges: ['特性', '修复'],
  // limit subject length
  subjectLimit: 100
}
