/**
 * @description 登录、获取用户信息、退出登录、清除token逻辑，不建议修改
 */
import tool from '@/utils/tool'

const state = () => ({
  userInfo: {},
  token: '',
  test: '123'
})

const mutations = {
  SET_USER_INFO(state, userInfo) {
    state.userInfo = tool.replacementFunction(state.userInfo, userInfo)
  },
  SET_TOKEN(state, token) {
    state.token = token
  }
}
const actions = {}
export default {
  state,
  mutations,
  actions
}
