import request from '@/utils/request'

export function getUserInfo(data) {
  return request.post('/mock/getUserInfo', data)
}
