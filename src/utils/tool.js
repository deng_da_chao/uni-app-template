function isYesterday(theDate) {
  var date = new Date()
  var today = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() // 今天凌晨
  var yesterday = new Date(today - 24 * 3600 * 1000).getTime()
  return theDate.getTime() < today && yesterday <= theDate.getTime()
}

/* —————————————— 工具集: uniapp版 ———————————————— */
export default {
  /**
   * @description   将时间转换为指定格式
   */

  timeFiltering(time) {
    let reg = /-/g
    if (!time) return
    time = time.replace(reg, '/')
    time = new Date(time)
    let nowTime = new Date()

    // 原始的时间
    let y = time.getFullYear()
    let m = time.getMonth() + 1
    m = m < 10 ? '' + m : m
    let d = time.getDate()
    d = d < 10 ? '' + d : d
    let h = time.getHours()
    h = h < 10 ? '0' + h : h
    let minute = time.getMinutes()
    let second = time.getSeconds()
    minute = minute < 10 ? '0' + minute : minute
    second = second < 10 ? '0' + second : second

    // 现在的时间
    let y1 = nowTime.getFullYear()
    let m1 = nowTime.getMonth() + 1
    m1 = m1 < 10 ? '' + m1 : m1
    let d1 = nowTime.getDate()
    d1 = d1 < 10 ? '' + d1 : d1
    let h1 = nowTime.getHours()
    h1 = h1 < 10 ? '0' + h1 : h1
    let minute1 = nowTime.getMinutes()
    let second1 = nowTime.getSeconds()
    minute1 = minute1 < 10 ? '0' + minute1 : minute1
    second1 = second1 < 10 ? '0' + second1 : second1

    // 其他 如果为当年 则显示 1月10日 12:01
    // 如果不为当年则显示 2020年12月10日 12:01
    if (nowTime.getFullYear() == y) {
      // 如果时间是今天 就显示 今天 12:32
      if (nowTime.getMonth() + 1 == m && nowTime.getDate() == d) {
        // 如果1小时内
        if (!(h1 - h)) {
          return `${minute1 - minute}分钟前`
        } else {
          // 大于1小时小于24小时
          return `${h1 - h}小时前`
        }
        // return "今天 " + h + ":" + minute + ":" + second;
      }
      // 如果时间是昨天 就显示 昨天 12:32
      if (isYesterday(time)) {
        return '昨天 '
      }
      return m + '月' + d + '日 '
    }
    return y + '年' + m + '月' + d + '日 '
  },

  /**
   * 判断是否为空  true带边空
   */
  isEmpty(a) {
    if (a === '') return true //检验空字符串
    if (a === 'null') return true //检验字符串类型的null
    if (a === 'undefined') return true //检验字符串类型的 undefined
    if (!a && a !== 0 && a !== '') return true //检验 undefined 和 null
    if (Array.prototype.isPrototypeOf(a) && a.length === 0) return true //检验空数组
    if (Object.prototype.isPrototypeOf(a) && Object.keys(a).length === 0) return true //检验空对象
    return false
  },

  /**JSON替换函数 */
  replacementFunction: function (oldJSON, newJSON) {
    let keysArray = Object.keys(oldJSON) //原来状态机里面储存的对象
    let keysArray2 = Object.keys(newJSON) //传入的对象

    if (keysArray.length && this.isEmpty(newJSON) == false) {
      keysArray.forEach((key) => {
        keysArray2.forEach((key2) => {
          if (key == key2) {
            oldJSON[key] = newJSON[key]
          } else {
            oldJSON[key2] = newJSON[key2]
          }
        })
      })
    } else {
      oldJSON = newJSON
    }
    return oldJSON
  },

  /**
   * 本算法来源于简书开源代码，详见：https://www.jianshu.com/p/fdbf293d0a85
   * 全局唯一标识符（uuid，Globally Unique Identifier）,也称作 uuid(Universally Unique IDentifier)
   * 一般用于多个组件之间,给它一个唯一的标识符,或者v-for循环的时候,如果使用数组的index可能会导致更新列表出现问题
   * 最可能的情况是左滑删除item或者对某条信息流"不喜欢"并去掉它的时候,会导致组件内的数据可能出现错乱
   * v-for的时候,推荐使用后端返回的id而不是循环的index
   * @param {Number} len uuid的长度
   * @param {Boolean} firstU 将返回的首字母置为"u"
   * @param {Nubmer} radix 生成uuid的基数(意味着返回的字符串都是这个基数),2-二进制,8-八进制,10-十进制,16-十六进制
   */
  guid: function (len = 32, radix = null) {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('')
    const uuid = []
    radix = radix || chars.length

    if (len) {
      // 如果指定uuid长度,只是取随机的字符,0|x为位运算,能去掉x的小数位,返回整数位
      for (let i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)]
    } else {
      let r
      // rfc4122标准要求返回的uuid中,某些位为固定的字符
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
      uuid[14] = '4'

      for (let i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | (Math.random() * 16)
          uuid[i] = chars[i === 19 ? (r & 0x3) | 0x8 : r]
        }
      }
    }
    return uuid.join('')
  }
}
