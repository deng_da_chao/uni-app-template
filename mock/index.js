import Mock from 'mockjs'

let mockapiList = []
const mocksContext = require.context('./modules/', true, /.js$/)
mocksContext.keys().forEach((file_name) => {
  // 获取文件中的 default 模块
  const mocks = mocksContext(file_name)
  for (const mock of mocks) {
    mockapiList.push(mock)
  }
})

/**
 * 描述
 * @author ChenJunHong
 * @date 2021-03-08
 * @param {any} reqUrl
 * @param {any} data
 * @returns {any} 返回模拟数据
 */
export function resMockData(reqUrl, data) {
  let res = {}
  mockapiList.forEach((e) => {
    if (e.url == reqUrl) {
      res = e.result(data)
    }
  })
  return res
}
