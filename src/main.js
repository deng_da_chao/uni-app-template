import Vue from 'vue'
import App from './App'
import store from './stores/index'

// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView)

Vue.config.productionTip = false

import * as filters from '@/filters'
// register global utility filters
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

App.mpType = 'app'

const app = new Vue({
  ...App,
  store
})

Vue.mixin({
  computed: {
    theme() {
      return this.$store.state.theme
    }
  }
})

app.$mount()
