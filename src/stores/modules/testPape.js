/**
 * @description 试卷信息
 */

const state = () => ({
  testInfo: [],
  question_id: '',
  word_id: '',
  title: ''
})

const mutations = {
  SET_TEST_INFO(state, testInfo) {
    state.testInfo = testInfo
  },
  SET_QUESTION_ID(state, question_id) {
    state.question_id = question_id
  },
  SET_WORD_ID(state, word_id) {
    state.word_id = word_id
  },
  SET_TITLE(state, title) {
    state.title = title
  }
}

const actions = {}
export default {
  state,
  mutations,
  actions
}
