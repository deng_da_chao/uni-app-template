/**
 * @description 导入所有 vuex 模块，自动加入namespaced:true，用于解决vuex命名冲突，请勿修改。
 */
import Vue from 'vue'
import Vuex from 'vuex'
import createVuexAlong from 'vuex-along'
import wxadapter from 'vuex-along-wx-adapter'
import theme from '@/styles/theme.scss'

Vue.use(Vuex)
const files = require.context('./modules', false, /\.js$/)
const modules = {}
files.keys().forEach((key) => {
  modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})
Object.keys(modules).forEach((key) => {
  modules[key]['namespaced'] = true
})

const dataState = createVuexAlong({
  // 设置保存的集合名字，避免同站点下的多项目数据冲突
  name: 'appData',
  local: {
    list: ['user'] // 将test的num属性存储到local
  },
  session: {
    list: [] // 将整个user存储到session
  },
  // #ifdef MP-WEIXIN
  adapterOptions: wxadapter()
  // #endIf
})

const store = new Vuex.Store({
  state: {
    theme: theme
  },
  modules,
  plugins: [dataState]
})

export default store
