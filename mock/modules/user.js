import Mock from 'mockjs'

module.exports = [
  {
    url: '/mock/getUserInfo',
    result: (data) => {
      console.log('data', data)
      return {
        code: 200,
        msg: 'success',
        data: Mock.mock({
          'string|1-10': '★'
        })
      }
    }
  }
]
